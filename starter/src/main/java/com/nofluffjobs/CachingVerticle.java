/*
 * CachingVerticle powinien w odstępach 60 sekundowych wywoływać endpoint 
 * REST-owy GET /posting, deserializować i zapisywać w pamięci listę ogłoszeń 
 * (dzięki temu mamy potencjalnie szybszy dostęp do ogłoszeń niż ogłoszenia 
 * wczytywane z dysku przez PostingVerticle). 
 */
package com.nofluffjobs;

import com.github.davidmoten.rx.RetryWhen;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.nofluffjobs.domain.Posting;
import com.nofluffjobs.util.DefaultJitter;
import com.nofluffjobs.util.ExpBackoff;
import io.vertx.rxjava.core.AbstractVerticle;
import io.vertx.rxjava.ext.web.client.HttpResponse;
import io.vertx.rxjava.ext.web.client.WebClient;
import io.vertx.rxjava.ext.web.codec.BodyCodec;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import rx.Single;

/**
 *
 * @author Sebastian
 */
public class CachingVerticle extends AbstractVerticle {

    private List<Posting> postings = new ArrayList<>();

    @Override
    public void start() {

        WebClient client = WebClient.create(vertx);
        Single<HttpResponse<String>> request = client.get(8080, "localhost", "/posting")
                .as(BodyCodec.string())
                .rxSend();

        vertx.setPeriodic(60000, id -> {

            request.subscribe(ar -> {

                if (ar.statusCode() == 200) {
                    // Deserialization
                    Gson gson = new Gson();
                    Type type = new TypeToken<List<Posting>>() {
                    }.getType();

                    postings = gson.fromJson(ar.body(), type);

                    System.out.println("CachingVerticle::Received postings " + postings.size());
                } else {
                    System.out.println("CachingVerticle::Something went wrong " + ar.statusMessage());
                }
            }, error -> System.out.println("Error!"));

            request.retryWhen(RetryWhen.exponentialBackoff(1, TimeUnit.SECONDS).build());

        });

    }
}
