/*
 * Starter to będzie startowy Verticle, który dokona deploymentu 
 * PostingVerticle i CachingVerticle
 */
package com.nofluffjobs;

import com.nofluffjobs.util.Runner;
import io.vertx.core.AbstractVerticle;

/**
 *
 * @author Sebastian
 */
public class Starter extends AbstractVerticle {

    // Convenience method so you can run it in your IDE
    public static void main(String[] args) {
        Runner.runApp(Starter.class);
    }

    @Override
    public void start() throws Exception {

        System.out.println("Starter verticle has started");

        // Deploy CachingVerticle instance
        vertx.deployVerticle("com.nofluffjobs.CachingVerticle", res -> {

            if (res.succeeded()) {

                String deploymentID = res.result();

                System.out.println("CachingVerticle deployed ok, deploymentID = " + deploymentID);

            } else {
                res.cause().printStackTrace();
            }
        });

        // Deploy PostingVerticle instance
        vertx.deployVerticle("com.nofluffjobs.PostingVerticle", res -> {

            if (res.succeeded()) {

                String deploymentID = res.result();

                System.out.println("PostingVerticle deployed ok, deploymentID = " + deploymentID);

            } else {
                res.cause().printStackTrace();
            }
        });

    }
}
