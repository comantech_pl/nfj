/*
 * PostingVerticle powinien udostępnić dwa endpointy REST:

 * - POST /posting - służy do dodawania nowego ogłoszenia
 * - GET /posting - służy do zwracania listy ogłoszeń
 */
package com.nofluffjobs;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.nofluffjobs.domain.Posting;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.file.AsyncFile;
import io.vertx.core.file.OpenOptions;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.rx.java.RxHelper;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import rx.Observable;

/**
 *
 * @author Sebastian
 */
public class PostingVerticle extends AbstractVerticle {

    private static final String FILE_NAME = "postings.json";
    private List<Posting> INIT_DATA = Arrays.asList(
            new Posting("Title", "Company", 0, 0, "City", "Street", "postalCode")
    );

    @Override
    public void start() {

        Router router = Router.router(vertx);
        router.route().handler(BodyHandler.create());

        router.get("/posting").handler(this::handleList);
        router.post("/posting").handler(this::handleAdd);

        vertx.createHttpServer().requestHandler(router::accept).listen(8080);

        Gson gson = new Gson();
        // Write a file
        vertx.fileSystem().open(FILE_NAME, new OpenOptions()
                .setAppend(false)
                .setCreate(true), ar -> {

            if (ar.succeeded()) {

                AsyncFile asyncFile = ar.result();
                String json = gson.toJson(INIT_DATA);

                Buffer chunk = Buffer.buffer(json);
                asyncFile.write(chunk);
            } else {
                System.err.println("Could not open file");
            }
        });

    }

    private void handleAdd(RoutingContext routingContext) {

        HttpServerResponse response = routingContext.response();
        Gson gson = new Gson();
        // Deserialization
        Posting posting = gson.fromJson(routingContext.getBodyAsJson().toString(), Posting.class);

        if (posting == null) {
            response.setStatusCode(400).end();
        } else {
            // Write a file
            vertx.fileSystem().open(FILE_NAME, new OpenOptions()
                    .setAppend(false)
                    .setCreate(false), ar -> {

                if (ar.succeeded()) {

                    AsyncFile asyncFile = ar.result();
                    Observable<Buffer> observable = RxHelper.toObservable(asyncFile);

                    observable.forEach(data -> {
                        Type type = new TypeToken<List<Posting>>() {
                        }.getType();
                        List<Posting> list = gson.fromJson(data.toString("UTF-8"), type);
                        list.add(posting);
                        String json = gson.toJson(list);

                        Buffer chunk = Buffer.buffer(json);
                        asyncFile.write(chunk);

                        // Response about
                        routingContext
                                .response()
                                .putHeader("content-type", "application/json")
                                .end(posting.getTitle() + " Added");
                    });;

                } else {
                    System.err.println("Could not open file");
                }
            });
        }
    }

    private void handleList(RoutingContext routingContext) {

        vertx.fileSystem().open(FILE_NAME, new OpenOptions()
                .setCreate(false), ar -> {

            if (ar.succeeded()) {

                AsyncFile file = ar.result();
                Observable<Buffer> observable = RxHelper.toObservable(file);

                Gson gson = new Gson();
                JsonArray arr = new JsonArray();

                observable.forEach(data -> {
                    routingContext.response().putHeader("content-type", "application/json").end(data.toString());
                });
            } else {
                System.err.println("Could not open file");
            }

        });
    }

}
