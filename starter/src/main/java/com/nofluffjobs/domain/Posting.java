package com.nofluffjobs.domain;

import java.util.Objects;

/**
 *
 * @author Sebastian
 */
public class Posting {

    private String title;
    private String company;
    private int salaryMin;
    private int salaryMax;
    private String city;
    private String street;
    private String postalCode;

    public Posting(String title, String company, int salaryMin, int salaryMax, String city, String street, String postalCode) {
        this.title = title;
        this.company = company;
        this.salaryMin = salaryMin;
        this.salaryMax = salaryMax;
        this.city = city;
        this.street = street;
        this.postalCode = postalCode;
    }

    public Posting() {
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the company
     */
    public String getCompany() {
        return company;
    }

    /**
     * @param company the company to set
     */
    public void setCompany(String company) {
        this.company = company;
    }

    /**
     * @return the salaryMin
     */
    public int getSalaryMin() {
        return salaryMin;
    }

    /**
     * @param salaryMin the salaryMin to set
     */
    public void setSalaryMin(int salaryMin) {
        this.salaryMin = salaryMin;
    }

    /**
     * @return the salaryMax
     */
    public int getSalaryMax() {
        return salaryMax;
    }

    /**
     * @param salaryMax the salaryMax to set
     */
    public void setSalaryMax(int salaryMax) {
        this.salaryMax = salaryMax;
    }

    /**
     * @return the city
     */
    public String getCity() {
        return city;
    }

    /**
     * @param city the city to set
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * @return the street
     */
    public String getStreet() {
        return street;
    }

    /**
     * @param street the street to set
     */
    public void setStreet(String street) {
        this.street = street;
    }

    /**
     * @return the postalCode
     */
    public String getPostalCode() {
        return postalCode;
    }

    /**
     * @param postalCode the postalCode to set
     */
    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + Objects.hashCode(this.title);
        hash = 89 * hash + Objects.hashCode(this.company);
        hash = 89 * hash + this.salaryMin;
        hash = 89 * hash + this.salaryMax;
        hash = 89 * hash + Objects.hashCode(this.city);
        hash = 89 * hash + Objects.hashCode(this.street);
        hash = 89 * hash + Objects.hashCode(this.postalCode);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Posting other = (Posting) obj;
        if (this.salaryMin != other.salaryMin) {
            return false;
        }
        if (this.salaryMax != other.salaryMax) {
            return false;
        }
        if (!Objects.equals(this.title, other.title)) {
            return false;
        }
        if (!Objects.equals(this.company, other.company)) {
            return false;
        }
        if (!Objects.equals(this.city, other.city)) {
            return false;
        }
        if (!Objects.equals(this.street, other.street)) {
            return false;
        }
        return Objects.equals(this.postalCode, other.postalCode);
    }

    @Override
    public String toString() {
        return "Posting{" + "title=" + title + ", company=" + company + ", salaryMin=" + salaryMin + ", salaryMax=" + salaryMax + ", city=" + city + ", street=" + street + ", postalCode=" + postalCode + '}';
    }

}
