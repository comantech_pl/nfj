/*
 * W CachingVerticle, przy wywołaniu endpointu GET /posting należy 
 * zaimplementować Exponential-Backoff z uwzględnieniem Jitter 
 * (żeby uchronić się przed thundering herd problem).
 */
package com.nofluffjobs.util;

import io.reactivex.Observable;
import io.reactivex.functions.BiFunction;
import java.util.concurrent.TimeUnit;
import rx.functions.Func1;

/**
 *
 * @author Sebastian
 */
public class ExpBackoff implements Func1<Observable<? extends Throwable>, Observable<Long>> {

    private final Jitter jitter;
    private final long delay;
    private final TimeUnit units;
    private final int retries;

    public ExpBackoff(Jitter jitter, long delay, TimeUnit units, int retries) {
        this.jitter = jitter;
        this.delay = delay;
        this.units = units;
        this.retries = retries > 0 ? retries : Integer.MAX_VALUE;
    }

    private long getNewInterval(int retryCount) {
        long newInterval = (long) (delay * Math.pow(retryCount, 2) * jitter.get());
        if (newInterval < 0) {
            newInterval = Long.MAX_VALUE;
        }
        return newInterval;
    }

    @Override
    public Observable<Long> call(Observable<? extends Throwable> observable) {
        return observable
                .zipWith(
                        Observable.range(1, retries),
                        (BiFunction<Throwable, Integer, Integer>) (throwable, retryCount) -> retryCount)
                .flatMap((attemptNumber) -> Observable.timer(getNewInterval(attemptNumber), units));
    }
}
