package com.nofluffjobs.util;

import java.util.Random;

/**
 *
 * @author Sebastian
 */
public class DefaultJitter implements Jitter {

    private final Random random = new Random();

    /**
     * Returns a random value inside [0.85, 1.15] every time it's called
     * @return 
     */
    @Override
    public double get() {
        return 0.85 + random.nextDouble() % 0.3f;
    }
}
