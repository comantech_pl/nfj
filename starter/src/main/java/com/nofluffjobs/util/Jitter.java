package com.nofluffjobs.util;

/**
 *
 * @author Sebastian
 */
public interface Jitter {

    double get();

    Jitter NO_OP = () -> 1;
}
